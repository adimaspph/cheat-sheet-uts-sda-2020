import java.util.Arrays;

public class LinkedList {
    public static void main(String[] args) {
        // test
        DoublyLinkedList linkedList = new DoublyLinkedList();

        int []arr = {12, 34, 54, 2, 3, 100 , 1000, 13513, 112, 9 ,234 ,3 };

        for (int x : arr) {
            linkedList.addFirst(x);
        }

        System.out.println(linkedList);

        linkedList.removeFirst();

        System.out.println(linkedList);

        linkedList.removeLast();
        System.out.println(linkedList);

    }


    // Doubly linked list dengan method addFirst, addLast, removeLast, removeFirst
    static class DoublyLinkedList<E extends Object> {
        ListNode header;
        ListNode footer;

        public DoublyLinkedList() {
            this.header = new ListNode();
            this.footer = header;
        }

        public void addLast(E data) {
            ListNode newNode = new ListNode(data, footer, null);
            footer.next = newNode;
            footer = newNode;
        }

        public void addFirst(E data) {
            if (header.next == null) {
                addLast(data);
            } else {
                ListNode newNode = new ListNode(data, header, header.next);
                header.next.prev = newNode;
                header.next = newNode;
            }

        }

        public void removeFirst() {
            ListNode firstItem = header.next;

            if (firstItem != null) {
                header.next = firstItem.next;

                if (firstItem.next != null) {
                    firstItem.next.prev = header;
                }
            }
        }

        public void removeLast() {
            ListNode lastItem = footer;

            if (lastItem.prev != header) {
                lastItem.prev.next = null;
                footer = lastItem.prev;
            }
        }

        // Mengoverride toString default
        public String toString() {
            StringBuilder result = new StringBuilder();

            ListNode pointer = header;

            while (pointer.next != null) {
                pointer = pointer.next;
                result.append(pointer.data + " ");
            }

            return result.toString();
        }
    }


    static class ListNode<E extends Object> {
        E data;
        ListNode next;
        ListNode prev;

        public ListNode(E data, ListNode prev, ListNode next) {
            this.data = data;
            this.prev = prev;
            this.next = next;
        }

        public ListNode() {
            this (null, null,null);
        }

    }
}
